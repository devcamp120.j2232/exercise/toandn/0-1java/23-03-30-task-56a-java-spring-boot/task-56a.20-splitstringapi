package com.devcamp.api.split_string_api.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class SplitStringController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(value="inputString", defaultValue = "mot hai ba bon") String input) {
        ArrayList<String> listString = new ArrayList<String>();
        String[] arrString = input.split(" ");

        listString = new ArrayList<>(Arrays.asList(arrString));
        return listString;
    }
}
